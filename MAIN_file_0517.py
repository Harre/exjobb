# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import RPi.GPIO as GPIO
import matplotlib.pyplot as plt
from Adafruit_PWM_Servo_Driver import PWM
from Definitions_file import offset_calculation
from Definitions_file import ir_scan

#--------------------------------------------------------------------------
#INITIALIZE:

GPIO.setmode(GPIO.BCM)

# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25
 
# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)

# Initialize the PWM device using the default address
pwm = PWM(0x40)
# Set frequency to 50 Hz
pwm.setPWMFreq(50)

# define the lower and upper boundaries of the "green" in BGR
# converts later to the HSV color space
greenLower = (29, 100, 40)
greenUpper = (64, 255, 240)

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)

#--------------------------------------------------------------------------
#MAIN:

# capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    # grab the raw NumPy array representing the image, then initialize the timestamp
    # and occupied/unoccupied text
    frame = frame.array

    # blur the frame to reduce high frequency noise
    frame_blur = cv2.GaussianBlur(frame, (11, 11), 0)

    # convert the blurred frame to HSV color space
    hsv = cv2.cvtColor(frame_blur, cv2.COLOR_BGR2HSV)

    # construct a mask for the color "green", then perform
    # a series of dilations and erosions to remove any small
    # blobs left in the mask
    mask = cv2.inRange(hsv, greenLower, greenUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask= mask)

    # find contours in the mask and initialize the current
    # (x, y) center of the ball
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    center = None

    # only proceed if at least one contour was found
    if len(cnts) > 0:
        # find the largest contour in the mask, then use
        # it to compute the minimum enclosing circle and centroid
        c = max(cnts, key=cv2.contourArea)
        ((xk, yk), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

        ball_pos_y = 480 - yk
        ball_pos_x = xk

        # only proceed if the radius meets a minimum size
        if radius > 10:
            # draw the circle and centroid on the frame,  
            cv2.circle(frame, (int(xk), int(yk)), int(radius), (0, 0, 255), 2)
            cv2.circle(frame, center, 5, (0, 0, 255), -1)
            ball_pos_ir_y = offset_calculation(ball_pos_y,ball_pos_x)[0]
            ball_pos_ir_x = offset_calculation(ball_pos_y,ball_pos_x)[1]
            print "center, radius: {}, {}, {}, IR position: {}, {}".format(int(ball_pos_x),int(ball_pos_y),int(radius),int(ball_pos_ir_x),int(ball_pos_ir_y) )

    # show the frames
    camera.hflip=True
    camera.vflip=True
    cv2.imshow("Frame", frame)
    cv2.imshow("blur", frame_blur)
    cv2.imshow("mask", mask)
    cv2.imshow("res", res)
    key = cv2.waitKey(1) & 0xFF

    # scanning and plot
    if key == ord("a"):
        Matrix = ir_scan(radius, ball_pos_ir_y, ball_pos_ir_x)
        
        # plot matrix
        im = plt.imshow(Matrix, origin='lower', cmap = 'hot')
        plt.colorbar(im, orientation = 'horizontal')
        plt.show()

    # clear the stream in preparation for the next frame
    rawCapture.truncate(0)

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break