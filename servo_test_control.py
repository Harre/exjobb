#Servo controll
from Adafruit_PWM_Servo_Driver import PWM
import time
import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
pwm = PWM(0x40)

def setServoPulse(channel, pulse):
  pulseLength = 1000000                   # 1,000,000 us per second
  pulseLength /= 60                       # 60 Hz
  print "%d us per period" % pulseLength
  pulseLength /= 4096                     # 12 bits of resolution
  print "%d us per bit" % pulseLength
  pulse *= 1000
  pulse /= pulseLength
  pwm.setPWM(channel, 0, pulse)

pwm.setPWMFreq(50)

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)

x = 295

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        # grab the raw NumPy array representing the image, then initialize the timestamp
        # and occupied/unoccupied text
        frame = frame.array

        key = cv2.waitKey(1) & 0xFF

        
        pwm.setPWM(1, 0, x) #Horizontal
        if key == ord("s"):
                x = 332
        if key == ord("a"):
                x = 259
        if key == ord("x"):
                x = 358
        if key == ord("z"):
                x = 232
        if key == ord("n"):
                x = 102
        if key == ord("m"):
                x = 512 #578
        if key == ord("k"):
                x = x+1
        if key == ord("j"):
                x = x-1

        print "x: {}".format(x)
        camera.hflip=True
        camera.vflip=True
        cv2.imshow("Frame", frame)

        # clear the stream in preparation for the next frame
        rawCapture.truncate(0)

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
                break
