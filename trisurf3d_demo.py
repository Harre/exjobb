from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

n_angles = 36
n_radii = 8

# An array of radii
# Does not include radius r=0, this is to eliminate duplicate points
radii = np.linspace(0.125, 1.0, n_radii)

# An array of angles
angles = np.linspace(0, 2*np.pi, n_angles, endpoint=False)

# Repeat all angles for each radius
angles = np.repeat(angles[..., np.newaxis], n_radii, axis=1)

# Convert polar (radii, angles) coords to cartesian (x, y) coords
# (0, 0) is added here. There are no duplicate points in the (x, y) plane
#x = np.append(0, (radii*np.cos(angles)).flatten())
#y = np.append(0, (radii*np.sin(angles)).flatten())

# Pringle surface
#z = np.sin(-x*y)

#a=np.array([[1,2,3],[4,5,6],[7,8,9]])
a = [[0 for x in range(3)] for x in range(3)] 
a[0][0] = 1
a[0][1] = 2
a[0][2] = 3
a[1][0] = 4


x = np.linspace(0, 1, 2, 3, 4)
y = np.linspace(0, 1, 2, 3, 4)
#y = [0,1,2]

#z = a[int(0)][int(0)]

z = a[(int(x))][(int(y))]

fig = plt.figure()
ax = fig.gca(projection='3d')

ax.plot_trisurf(x, y, z, cmap=cm.jet, linewidth=0.2)

plt.show()


