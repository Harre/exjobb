def ir_scan(radius, ball_pos_ir_y, ball_pos_ir_x):
        # IR signal
        Recieved_signal_from_ir = True

        # Translate camera position to IR and create the area that will be scanned
        ticksminx=231.84
        ticksminy=271.1
        radius=radius+1
        xmin = int( ball_pos_ir_x+(-radius)*126.32/640 +ticksminx)
        xmax = int( ball_pos_ir_x+(radius)*126.32/640 +ticksminx)
        ymin = int( ball_pos_ir_y+(-radius)*97.8/480 +ticksminy)
        ymax = int( ball_pos_ir_y+(radius)*97.8/480 +ticksminy)

        # Scanning begin in the upper left corner
        xir = xmin
        yir = ymin
        z = 1

        # Create a matrix containing the position and distance data from the IR sensor
        # Creates a list containing 5 lists initialized to 0
        Matrix = [[0 for x in range(xmax-xmin+1)] for x in range(ymax-ymin+1)]

        while yir <= ymax:
 
                #time.sleep(0.2)
                pwm.setPWM(0, 0, yir) #Vertical
                pwm.setPWM(1, 0, xir) #Horizontal
                time.sleep(0.5)

                if Recieved_signal_from_ir:       #Wait for a distance reading from the ir sensor
                        Matrix[yir-ymin][xir-xmin] = distance_from_ir()
                        print "x,y,distance: {}, {}, {}mm".format(xir-xmin,yir-ymin,distance_from_ir())
                        xir = xir + z

                if xir > xmax or xir < xmin:      # At end of horizontal scan line
                        yir = yir + 1             # take a step upward
                        z = - z                   # Change scan direction
                        xir = xir + z  

        # standard direction                 
        time.sleep(0.5)
        pwm.setPWM(0, 0, 320) #Vertical
        pwm.setPWM(1, 0, 295) #Horizontal
        
        return Matrix
