import math
vertical_pixel = 480 #Vertical pixel position of the object seen from the camera (0-480)
horizontal_pixel = 640  #Horizontal pixel position of the object seen from the camera (0-640)
L = 1000.0    #Length to object
alpha_deg = 20.7    #Half the vertical view of the camera in degrees
alpha = alpha_deg*(math.pi/180) #in radians
B = 82.0 #Distance between the IR sensor and the camera
A = L * math.tan(alpha) * ((vertical_pixel-240)/240.0)  #Half of the opposite side of the camera view
C = math.sqrt(L**2+B**2)    #Length of the IR centerline to the object
beta = math.atan(B/L)  #Angle between the camera and IR sensor in radians
x = math.pi/2 - beta    #Angle between IR center line and vertical lower angle
d = beta + math.pi/2    #Angle between IR centerline and vertical upper angle

D = math.sqrt(C**2+A**2-2*C*A*math.cos(d))   #Length of the upmost line of the IR
fi1 = math.asin((A/D)*math.sin(d))  #Angle between the upmost and centerline of the IR

E = math.sqrt(C**2+A**2-2*C*A*math.cos(x))  #Length of the downmost line of the IR
fi2 = math.asin((A/E)*math.sin(x))  #Angle between the downmost and centerline of the IR



if vertical_pixel >= 240:
    #From zero angle as starting position for the IR
    fi11 = math.atan((A+B)/L)
    vertical_degrees_upper_triangle = fi11 * 180/math.pi
    vertical_tick = int(round(vertical_degrees_upper_triangle * 425/180 + 97.77/2 - 9.4))  #Position of the object seen from the IR


if vertical_pixel < 240:
    fi22 = math.atan((A+B)/L)
     #From zero angle as starting position for the IR
    vertical_degrees_lower_triangle = (fi22)*(180/math.pi)
    vertical_tick = int(round(vertical_degrees_lower_triangle * 425/180 + 97.77/2 - 9.96))  #Position of the object seen from the IR
    
    
horizontal_tick = int(round(horizontal_pixel*(425*53.5/180)/640))   #Position of the object seen from the IR
#425 ticks corresponds to 180 degrees. 53.5 degrees horizontal view over 640 pixels gives a tick interval of 126.

print(fi22*(180/math.pi))
#print(fi2*(180/math.pi))
print(fi11*(180/math.pi))
#print(fi1*(180/math.pi))

print(vertical_tick)
print(horizontal_tick)

