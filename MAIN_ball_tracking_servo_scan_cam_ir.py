# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
from collections import deque
import numpy as np
import argparse
import imutils
import os
import RPi.GPIO as GPIO
import matplotlib.pyplot as plt
from Adafruit_PWM_Servo_Driver import PWM
import math
import camera_pixel_to_ir_tick_file

#--------------------------------------------------------------------------
# DEFINITONS:
 
# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)
 
        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low
 
        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
 
        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1
 
        GPIO.output(cspin, True)
        
        adcout >>= 1       # first bit is 'null' so drop it
        return adcout
      
def distance_from_ir():
        """
        # take ten readings and find the average (a) - this smooths things out a little.
        r=[]
        for i in range (0,10):
        r.append(value)
        a = sum(r)/float(10)
        """
        # read the analog pin
        ir_value = readadc(potentiometer_adc, SPICLK, SPIMOSI, SPIMISO, SPICS)
        
        # convert analog pin to voltage (v)
        v = (ir_value/1023.0)*3.3

        # use the magic formula to get the distance (d)
        distance = 16.2537 * v**4 - 129.893 * v**3 + 382.268 * v**2 - 512.611 * v + 306.439

        # distance from cm to mm
        distance = distance*10
        distance = int(distance)

        #print "distance: {} mm ".format(distance)
        return distance

"""
def setServoPulse(channel, pulse):
        pulseLength = 1000000                   # 1,000,000 us per second
        pulseLength /= 60                       # 60 Hz
        print "%d us per period" % pulseLength
        pulseLength /= 4096                     # 12 bits of resolution
        print "%d us per bit" % pulseLength
        pulse *= 1000
        pulse /= pulseLength
        pwm.setPWM(channel, 0, pulse)
"""

def scan(radius, yk, xk):
        # IR signal
        Recieved_signal_from_ir = True

        # Translate camera position to IR and create the area that will be scanned
        ticksminx=231.84
        ticksminy=271.1
        radius=radius+1
        xmin = int( xk+(-radius)*126.32/640 +ticksminx)
        xmax = int( xk+(radius)*126.32/640 +ticksminx)
        ymin = int( yk+(-radius)*97.8/480 +ticksminy)
        ymax = int( yk+(radius)*97.8/480 +ticksminy)

        # Scanning begin in the upper left corner
        xir = xmin
        yir = ymin
        z = 1

        # Create a matrix containing the position and distance data from the IR sensor
        # Creates a list containing 5 lists initialized to 0
        Matrix = [[0 for x in range(xmax-xmin+1)] for x in range(ymax-ymin+1)]

        while yir <= ymax:
 
                #time.sleep(0.2)
                pwm.setPWM(0, 0, yir) #Vertical
                pwm.setPWM(1, 0, xir) #Horizontal
                time.sleep(0.5)

                if Recieved_signal_from_ir:       #Wait for a distance reading from the ir sensor
                        Matrix[yir-ymin][xir-xmin] = distance_from_ir()
                        print "x,y,distance: {}, {}, {}mm".format(xir-xmin,yir-ymin,distance_from_ir())
                        xir = xir + z

                if xir > xmax or xir < xmin:      # At end of horizontal scan line
                        yir = yir + 1             # take a step upward
                        z = - z                   # Change scan direction
                        xir = xir + z  

        # standard direction                 
        time.sleep(0.5)
        pwm.setPWM(0, 0, 320) #Vertical
        pwm.setPWM(1, 0, 295) #Horizontal
        
        return Matrix


#--------------------------------------------------------------------------
#INITIALIZE:

GPIO.setmode(GPIO.BCM)

# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25
 
# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)

potentiometer_adc = 0

# Initialize the PWM device using the default address
pwm = PWM(0x40)
# Set frequency to 50 Hz
pwm.setPWMFreq(50)

# define the lower and upper boundaries of the "green"
# ball in the HSV color space, then initialize the
greenLower = (29, 86, 6)
greenUpper = (64, 255, 255)

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)

#--------------------------------------------------------------------------
#MAIN:

# capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        # grab the raw NumPy array representing the image, then initialize the timestamp
        # and occupied/unoccupied text
        frame = frame.array

        # blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # construct a mask for the color "green", then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        mask = cv2.inRange(hsv, greenLower, greenUpper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        # Bitwise-AND mask and original image
        res = cv2.bitwise_and(frame,frame, mask= mask)

        # find contours in the mask and initialize the current
        # (x, y) center of the ball
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                cv2.CHAIN_APPROX_SIMPLE)[-2]
        center = None

        # only proceed if at least one contour was found
        if len(cnts) > 0:
                # find the largest contour in the mask, then use
                # it to compute the minimum enclosing circle and
                # centroid
                c = max(cnts, key=cv2.contourArea)
                ((xk, yk), radius) = cv2.minEnclosingCircle(c)
                M = cv2.moments(c)
                center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

                ball_pos_y = 480 - yk
                ball_pos_x = xk


                # only proceed if the radius meets a minimum size
                if radius > 10:
                        # draw the circle and centroid on the frame,  
                        cv2.circle(frame, (int(xk), int(yk)), int(radius),
                                (0, 0, 255), 2)
                        cv2.circle(frame, center, 5, (0, 0, 255), -1)
                        print "center, radius: {}, {}, {}, IR position: {}, {}".format(int(ball_pos_x),int(ball_pos_y),int(radius),int(offset_calculation(ball_pos_y, ball_pos_x)[1]),int(offset_calculation(ball_pos_y, ball_pos_x)[0]) )

        # show the frames
        camera.hflip=True
        camera.vflip=True
        cv2.imshow("Frame", frame)
        cv2.imshow("mask", mask)
        cv2.imshow("res", res)
        key = cv2.waitKey(1) & 0xFF

        # scanning and plot
        if key == ord("a"):

                # offset calculations
                # scanning process
                # offset_calculation(ball_pos_y, ball_pos_x)[0] is the first returning value from offset_calculation(ball_pos_y, ball_pos_x) function
               ball_pos_ir_y = camera_pixel_to_ir_tick(ball_pos_y,ball_pos_x)[0]
               ball_pos_ir_x = camera_pixel_to_ir_tick(ball_pos_y,ball_pos_x)[1]
               Matrix = scan(radius, ball_pos_ir_y, ball_pos_ir_x)
                #Matrix = scan(radius, yk, xk)
                
                # plot matrix
                im = plt.imshow(Matrix, cmap = 'hot')
                plt.colorbar(im, orientation = 'horizontal')
                plt.show()

        # clear the stream in preparation for the next frame
        rawCapture.truncate(0)

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
                break

#--------------------------------------------------------------------------
