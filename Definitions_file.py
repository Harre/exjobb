import math
from Adafruit_PWM_Servo_Driver import PWM
import time
import RPi.GPIO as GPIO

def offset_calculation(verticalPixel, horizontalPixel):

    #verticalPixel = 480 #Vertical pixel position of the object seen from the camera (0-480)
    #horizontalPixel = 640  #Horizontal pixel position of the object seen from the camera (0-640)
    L = 500.0    #Length to object
    alphaDeg = 20.7    #Half the vertical view of the camera in degrees
    alpha = alphaDeg*(math.pi/180) #in radians
    B = 82.0 #Distance between the IR sensor and the camera
    A = L * math.tan(alpha) * ((verticalPixel-240)/240.0)  #Vertical offset to te object from the camera

    fi = math.atan((A+B)/L) #Vertical angle from zero angle of IR sensor to where object is.
    verticalDegreesTriangle = fi * 180/math.pi
    if verticalPixel >= 240:   #Upper triangle
        #From zero angle as starting position for the IR
        verticalTick = int(round(verticalDegreesTriangle * 425/180 + 97.77/2))  #Position of the object seen from the IR
        #97.77 is the vertical tick interval, this divided by 2 is added because we want to start counting from the middle of the interval.
        #-9.4 exist because the IR is set to match the lower angle of its interval. 97.77/2 makes it point to the middle of the interval
        #and -9.4 makes it have its zero tick at the lower angle of the camera picture.

    if verticalPixel < 240:    #Lower triangle
        #From zero angle as starting position for the IR
        verticalTick = int(round(verticalDegreesTriangle * 425/180 + 97.77/2))  #Position of the object seen from the IR

    horizontalTick = int(round(horizontalPixel*(425*53.5/180)/640))   #Position of the object seen from the IR
    #425 ticks corresponds to 180 degrees. 53.5 degrees horizontal view over 640 pixels gives a tick interval of 126.

    return (verticalTick, horizontalTick)


def ir_scan(radius, ball_pos_ir_y, ball_pos_ir_x):

    # Initialize the PWM device using the default address
    pwm = PWM(0x40)
    
    #Ignore distance readings that is less or more then the limits
    max_distance = 1400
    min_distance = 800
    x_compensation = 1
    y_compensation = 0

    # Translate camera position to IR and create the area that will be scanned
    ticksminx=231.84
    ticksminy=271.1
    radius=radius+5
    xmin = x_compensation + int( ball_pos_ir_x+(-radius)*126.32/640 +ticksminx)
    xmax = x_compensation + int( ball_pos_ir_x+(radius)*126.32/640 +ticksminx)
    ymin = y_compensation + int( ball_pos_ir_y+(-radius)*97.8/480 +ticksminy)
    ymax = y_compensation + int( ball_pos_ir_y+(radius)*97.8/480 +ticksminy)

    # Scanning begin in the upper left corner
    xir = xmin
    yir = ymin
    z = 1

    # Create a matrix containing the position and distance data from the IR sensor
    # Creates a list containing 5 lists initialized to 0
    Matrix = [[0 for x in range(xmax-xmin+1)] for x in range(ymax-ymin+1)]

    while yir <= ymax:

        pwm.setPWM(0, 0, yir) #Vertical
        pwm.setPWM(1, 0, xir) #Horizontal
        time.sleep(0.2)

        distance_reading = distance_from_ir()

        if distance_reading > max_distance:
            distance_reading = max_distance
        if distance_reading < min_distance:
            distance_reading = min_distance

        Matrix[yir-ymin][xir-xmin] = distance_reading
        print "x,y,distance: {}, {}, {}mm. distance from ir:{}".format(xir-xmin,yir-ymin,distance_reading,distance_from_ir())
        xir = xir + z

        if xir > xmax or xir < xmin:      # At end of horizontal scan line
            yir = yir + 1             # take a step upward
            z = - z                   # Change scan direction
            xir = xir + z

    # standard direction
    time.sleep(0.2)
    pwm.setPWM(0, 0, 320) #Vertical
    pwm.setPWM(1, 0, 295) #Horizontal

    return Matrix


# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
    if ((adcnum > 7) or (adcnum < 0)):
        return -1
    GPIO.output(cspin, True)

    GPIO.output(clockpin, False)  # start clock low
    GPIO.output(cspin, False)     # bring CS low

    commandout = adcnum
    commandout |= 0x18  # start bit + single-ended bit
    commandout <<= 3    # we only need to send 5 bits here
    for i in range(5):
        if (commandout & 0x80):
                GPIO.output(mosipin, True)
        else:
                GPIO.output(mosipin, False)
        commandout <<= 1
        GPIO.output(clockpin, True)
        GPIO.output(clockpin, False)

    adcout = 0
    # read in one empty bit, one null bit and 10 ADC bits
    for i in range(12):
        GPIO.output(clockpin, True)
        GPIO.output(clockpin, False)
        adcout <<= 1
        if (GPIO.input(misopin)):
                adcout |= 0x1

    GPIO.output(cspin, True)

    adcout >>= 1       # first bit is 'null' so drop it
    return adcout


def distance_from_ir():

    # change these as desired - they're the pins connected from the
    # SPI port on the ADC to the Cobbler
    SPICLK = 18
    SPIMISO = 23
    SPIMOSI = 24
    SPICS = 25

    adc = 0

    # read the analog pin
    ir_value = readadc(adc, SPICLK, SPIMOSI, SPIMISO, SPICS)

    # take ten readings and find the average (a) - this smooths things out a little.
    r=[]
    for i in range (0,5):
        r.append(ir_value)
        a = sum(r)/float(5)
        time.sleep(0.02)    #waiting for the new ir-sensor value (50Hz in time, 1/50=0.02)

    # convert analog pin to voltage (v)
    v = (ir_value/1023.0)*3.3

    # use the magic formula to get the distance (d)
    distance = 16.2537 * v**4 - 129.893 * v**3 + 382.268 * v**2 - 512.611 * v + 306.439

    # distance from cm to mm
    distance = distance*10/2
    distance = int(distance)

    return distance
