# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 11:25:49 2016

@author: Harre
"""
#The position of the ball in the camera image

xk=300
yk=300
radius=20

Recieved_signal_from_ir = True
distance_from_ir = 20

#Translate camera position to IR and create the area that will be scanned
xmin = int( (xk-radius)*61/640 )
xmax = int( (xk+radius)*61/640 )
ymin = int( (yk-radius)*47/480 )
ymax = int( (yk+radius)*47/480 )

#Scanning begin in the upper left corner
xir = xmin
yir = ymin
i = 1

#Create a matrix containing the position and distance data from the IR sensor
# Creates a list containing 5 lists initialized to 0
Matrix = [[0 for x in range(xmax-xmin+1)] for x in range(ymax-ymin+1)] 

while yir <= ymax:
    #pwm.setPWM(0, 0, yir) #Vertical
    #pwm.setPWM(1, 0, xir) #Horizontal
    
    if Recieved_signal_from_ir: #Wait for a distance reading from the ir sensor
        Matrix[yir-ymin][xir-xmin] = distance_from_ir
        print "x,y,distance: {}, {}, {}".format(xir-xmin,yir-ymin,distance_from_ir)
        xir = xir + i
        
    
    if xir > xmax or xir < xmin: #At end of horizontal scan line                         
        yir = yir + 1 #take a step upward
        i = - i #Change scan direction
        xir = xir + i
    